package br.ucsal.testequalidade20182.locadora;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import br.ucsal.testequalidade20182.locadora.dominio.Cliente;
import br.ucsal.testequalidade20182.locadora.dominio.Modelo;
import br.ucsal.testequalidade20182.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20182.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20182.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20182.locadora.persistence.VeiculoDAO;

public class LocacaoBOUnitarioTest {
	
	public void setup(){
		Cliente cliente = Mockito.spy(Cliente.class);
		Veiculo veiculo = Mockito.spy(Veiculo.class);
		
	}

	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. Método: public
	 * static Integer locarVeiculos(String cpfCliente, List<String> placas, Date
	 * dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observações: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		Date dataLocacao = new Date();
		Integer quantidadeDiasLocacao = 5;
		
		String placa = "ntc-0309";
		Veiculo veiculo = new Veiculo(placa,2019,new Modelo("Gol"), 50.20);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		List<String> placas = Arrays.asList(placa);
		
		String cpf = "";
		Cliente cliente = new Cliente(cpf,"Renato", "87654321");
		
		PowerMockito.mockStatic(ClienteDAO.class);
		PowerMockito.when(ClienteDAO.obterPorCpf(cpf)).thenReturn(cliente);
		
		PowerMockito.mockStatic(VeiculoDAO.class);
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo);
		
		
		
	}

	public static Integer locarVeiculos(String cpfCliente, List<String> placas, Date dataLocacao, Integer quantidadeDiasLocacao){
		
		
		return null;
	}
}
